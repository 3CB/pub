#!/bin/env python3
import valve.source.a2s
    # https://pypi.python.org/pypi/python-valve/0.1.1
    # https://python-valve.readthedocs.io/en/latest/source.html

address = ('3commandobrigade.com', 2363)

try:
        server = valve.source.a2s.ServerQuerier(address)
        players = server.get_players()

        if server.get_info()["player_count"] > 0:
            print("Duration\t\t\tName")
            for player in sorted(players["players"], key=lambda p: p["duration"], reverse=True):
                print("{duration}\t\t{name}".format(**player))
        else:
                print("No Player connected")
                exit(0)

except valve.source.a2s.NoResponseError:
        print("Cannot connect to", address)
        exit(1)
