# Mod Rename
# by reV for 3CB

Function Get-Directory {
	[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
	$FolderBrowserDialog = New-Object System.Windows.Forms.FolderBrowserDialog
	$FolderBrowserDialog.Description = "Select your Arma 3 Mod Directory"
	$FolderBrowserDialog.ShowDialog() | Out-Null
	$FolderBrowserDialog.SelectedPath
}

$LocalModPath = Get-Directory

Move-Item -Path "$LocalModPath\@ace3" -Destination "$LocalModPath\@ace" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@acex_a3" -Destination "$LocalModPath\@acex" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@rhs_afrf3" -Destination "$LocalModPath\@rhsafrf" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@rhs_gref" -Destination "$LocalModPath\@rhsgref" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@rhs_saf" -Destination "$LocalModPath\@rhssaf" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@rhs_usf3" -Destination "$LocalModPath\@rhsusaf" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@ace3_compat_afrf" -Destination "$LocalModPath\@ace_compat_rhs_afrf3" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@ace3_compat_gref" -Destination "$LocalModPath\@ace_compat_rhs_gref3" -Force -ErrorAction Continue
Move-Item -Path "$LocalModPath\@ace3_compat_usf" -Destination "$LocalModPath\@ace_compat_rhs_usf3" -Force -ErrorAction Continue



Write-Host 'Press any key to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');