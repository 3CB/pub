# Mod CleanUp
# by reV for 3CB

$confirmation = Read-Host "[d]elete, [a]sk or just [c]heck?"
	
Function Get-Repo {
	$url = "http://repo.3commandobrigade.com/" 
	$HTTPContent = Invoke-WebRequest $url
	$RepoList = $HTTPContent.Links | foreach {$_.innerText} | Select-String -Pattern "@[0-9a-zA-Z]"
	$RepoList -replace '/',''
}

Function Get-Directory {
	[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
	$FolderBrowserDialog = New-Object System.Windows.Forms.FolderBrowserDialog
	$FolderBrowserDialog.Description = "Select your Arma 3 Mod Directory"
	$FolderBrowserDialog.ShowDialog() | Out-Null
	$FolderBrowserDialog.SelectedPath
}

Function Get-LocalMods {
	$LocalModPath = Get-Directory
	$LocalList = Get-Childitem -Path $LocalModPath | foreach {$_.FullName} | Select-String -Pattern "\@[0-9a-zA-Z]"
	$LocalList
}

$RepoMods = Get-Repo
$LocalMods = Get-LocalMods

if ($RepoMods.Count -gt 0) {
	if ($LocalMods.Count -gt 0) {
		$LocalMods | ForEach-Object { 
			$name = "$_".split("\")[-1]
			if ( $RepoMods -notcontains $name ) {
				write-output "$_"
				if ($confirmation -eq 'd') {Remove-Item -Recurse -Force $_}
				elseif ($confirmation -eq 'a') {
					$DEL = Read-Host "Do you want to delete $name ? (y/n)"
					if ($DEL -eq 'y') {Remove-Item -Recurse -Force $_}
				}
			}
		}
	}
	else { Write-Host "Error: No Mods found in $LocalModPath" }
}
else { Write-Host "Error: No Mods found in $url" }

Write-Host 'Press any key to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');